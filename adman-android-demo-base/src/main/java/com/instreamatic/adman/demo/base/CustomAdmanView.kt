package com.instreamatic.adman.demo.base

import android.content.Context

import com.instreamatic.adman.view.BaseAdmanView


class CustomAdmanView(context: Context) : BaseAdmanView(context) {

    override fun defineIds() {
        layoutId = R.layout.demo_adman_custom_activity;
        bannerViewId = R.id.adman_banner
        restartViewId = R.id.adman_restart
        playViewId = R.id.adman_play
        pauseViewId = R.id.adman_pause
        leftViewId = R.id.adman_left
        closeViewId = R.id.adman_close
    }
}
