package com.instreamatic.adman.demo.base

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.content.res.Configuration
import android.util.Log

import com.instreamatic.adman.Adman
import com.instreamatic.adman.AdmanRequest
import com.instreamatic.adman.IAdman
import com.instreamatic.adman.Region
import com.instreamatic.adman.event.AdmanEvent
import com.instreamatic.adman.view.IAdmanView
import kotlinx.android.synthetic.main.demo_adman_activity.*


class DemoAdmanActivity : Activity(), AdmanEvent.Listener {
    private val TAG = "DemoAdman"

    private var adman: IAdman? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.demo_adman_activity)
        val request = AdmanRequest.Builder()
            .setSiteId(777)
            .setRegion(Region.EUROPE)
            .setPreview(14)
            .build()
        adman = Adman(this, request)
        adman?.bindModule(CustomAdmanView(this))
        adman?.addListener(this)

        start?.setOnClickListener(View.OnClickListener { adman?.start() })
    }

    override fun onPause() {
        super.onPause()
        adman?.pause()
    }

    override fun onResume() {
        super.onResume()
        adman?.play()
    }

    override fun onDestroy() {
        super.onDestroy()
        adman?.removeListener(this)
    }

    override  fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        (adman?.getModule(IAdmanView.ID) as IAdmanView).rebuild()
    }

    override fun onAdmanEvent(event: AdmanEvent) {
        Log.d(TAG, "onAdmanEvent: " + event.type.name)
        runOnUiThread {
            when (event.type) {
                AdmanEvent.Type.PREPARE -> {
                    start?.visibility = View.GONE
                    loading?.visibility = View.VISIBLE
                }
                AdmanEvent.Type.NONE, AdmanEvent.Type.FAILED, AdmanEvent.Type.COMPLETED -> {
                    start?.visibility = View.VISIBLE
                    loading?.visibility = View.GONE
                }
                AdmanEvent.Type.STARTED -> loading?.visibility = View.GONE
            }
        }
    }
}
